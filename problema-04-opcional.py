# bibliotecas
import gym
import gym_minigrid
import matplotlib.pyplot as plt
import numpy as np
# Funcion Monitor para luego poder generar un video con las secuencias de las acciones
from gym.wrappers.monitoring import stats_recorder, video_recorder
# set agent_view_size
from gym_minigrid.wrappers import ViewSizeWrapper

# env
env = gym.make('MiniGrid-LockedRoom-v0')

outdir = './output/'
# Guardar un video en la carpeta outdir
env = gym.wrappers.Monitor(env, outdir, force=True)
# set agent_view_size to 3 (default 7)
env = ViewSizeWrapper(env, 3)

# Ejemplo de ejecución de tres comandos
# Almacenar frames para luego graficar con matplotlib, se podrñía comentar y solo usar 
# Monitor
frames = []
# Genero el estado del sistema
obs= env.reset()

# Imprimo la matriz de 3x3 de observacion del agente, tener cuidado con los ejes
print("estado inicial del agente: ",obs)
# Primer frame
frames.append(env.render('rgb_array'))

# PRIMERA ACCION
#Roto a la derecha
action = env.actions.right
#Ejecuto la acción
obs, reward, done, info = env.step(action)
print("agente al girar a la derecha: ",obs)
# Guardo el estado del sistema en otro frame
frames.append(env.render('rgb_array'))

# SEGUNDA ACCION
#Adelante
action = env.actions.forward
#Ejecuto la acción
obs, reward, done, info = env.step(action)
print("agente al avanzar: ",obs)
# Guardo el estado del sistema en otro frame
frames.append(env.render('rgb_array'))

# TERCERA ACCION
# Levantar un objeto
action = env.actions.pickup
#Ejecuto la acción
obs, reward, done, info = env.step(action)
print("agente al tratar de levantar un objecto (si es que existe): ",obs)
# Guardo el estado del sistema en otro frame
frames.append(env.render('rgb_array'))

# Junto todos los frames para guardarlo en un png y mostar en pantalla, solo para debug
image = np.concatenate([frames[0], frames[1], frames[2], frames[3]], 1)
# save the image
plt.imsave(outdir+'test.png', image)
plt.imshow(image)
plt.show()