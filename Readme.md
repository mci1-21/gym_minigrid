# Evaluación Modulo 2: Programación Python, GIT, bash
## Sistema de navegación de un robot
### Fecha de exposición Jueves  10/06/21

Basado en [este proyecto](https://github.com/maximecb/gym-minigrid).

Para poner a prueba lo que aprendimos de Python en este proyecto representaremos un robot y otros objetos físicos en un mundo 2D empleando software con el propósito de planificar los movimientos del robot y alcanzar una posición objetivo. El movimiento del robot se llevará a cabo ejecutando ciertas acciones. Se pide implementar alguna estrategia (policy) de navegación que permita conducir el robot a la meta, sin emplear algoritmos de inteligencia artificial avanzados. 

### Crear & configurar el entorno de trabajo

1-  Crear el entorno de trabajo que denominaremos ```ml``` (o escoja algún nombre de su preferencia)

1.a- Usando Anaconda

- Descargar el software de Conda para su sistema operativo: 

* Reducido
[Miniconda](https://docs.conda.io/en/latest/miniconda.html)

* Completo
[Anaconda](https://www.anaconda.com/products/individual)

- Proceder con la instalación

- Una vez instalado debería estar accesible el comando ```conda``` y ejecutar 
```conda create -n ml python=3.7```

1.b- Cualquier gestor de entornos de trabajo

2- activar el entorno de trabajo ml (puede diferir con el sistema operativo)

```source activate ml```

3- instalar la bilioteca [gym](https://gym.openai.com/docs/)

```pip install gym```

4- instalar la bilioteca  [matplotlib](https://matplotlib.org/)

```pip install matplotlib```

5- clonar el repo del proyecto

```git clone https://gitlab.com/mci1-21/gym_minigrid```

6- Instalar la versión modificada de la bilioteca gym_minigrid

```cd gym_minigrid```

```pip install -e .```

7- probar la aplicacion de control manual de la aplicación

```python manual_control.py```

Con las flechas arriba, derecha e izquierda muevo el robot (no está implementado atrás), con espacio abro puertas y con pagup y pagdown recogo o dejo objetos (utilizado en otros mapas). El mapa cargado o env es ```MiniGrid-MultiRoom-N6-v0```. 

### Planteo y descripción del problema

Acciones del robot en el entorno:

- Girar a la izquierda: ```env.actions.left```

- Doble a la derecha: ```env.actions.right```

- Avanzar: ```env.actions.forward```

- Recoge un objeto: ```env.actions.pickup```

- Dejar caer el objeto que se está transportando: ```env.actions.drop```

- Alternar (abrir puertas, interactuar con objetos): ```env.actions.toggle```

- Hecho (Done) (tarea completada, opcional)

Al ejecutar una accion del robot ```obs, reward, done, info = env.step(action)```
podemos acceder a la variable ```obs``` que nos indica lo que está observando el robot. Se establece el rango de observación del robot de 3x3 (la configuración está en la línea ```env = ViewSizeWrapper(env, 3)```) para facilitar los cálculos. La matriz esta centrada en el campo de visión del robot, y la interpretación dependerá del eje del robot (dada por la dirección de la flecha roja). Cada elemento de esa matriz se corresponde con la posición de un mosaico en el mapa y codificada como una tupla tridimensional: ```(OBJECT_IDX, COLOR_IDX, STATE)```. En una primera aproximación nos interesará la componente de ```OBJECT_IDX``` que indica que objetos se encuentran en el campo de visión del robot (matrix de 3x3) y nos permitirá implementar la lógica de navegación.

- El mapeo ```OBJECT_TO_IDX``` y ```COLOR_TO_IDX``` se pueden encontrar en [gym_minigrid/minigrid.py](gym_minigrid/minigrid.py)
- p.ej. puerta ESTADO -> 0: abierta, 1: cerrada, 2: bloqueada

De forma predeterminada, se otorgan recompensas escasas por alcanzar una loseta de objetivo verde. En la implementación actual se otorga una recompensa de 1 por el éxito y cero por el fracaso. También hay un límite de paso de tiempo específico del entorno para completar la tarea. 

Codificación de la matriz de observación del agente OBJECT_IDX es 
```
'unseen'        : 0,
'empty'         : 1,
'wall'          : 2,
'floor'         : 3,
'door'          : 4,
'key'           : 5,
'ball'          : 6,
'box'           : 7,
'goal'          : 8,
'lava'          : 9,
'agent'         : 10,
```
Por otra parte la variable ```done``` nos indica si completamos el objetivo con ```true``` o ```false```.

Para debuggear se encuentra implementada la función ```Monitor``` de ```gym``` que genera un video en la carpeta dada por ```outdir```. También se encuentra implementado la opción de generar una secuencia de imágenes, conviente para analizar una limitada cantidad de pasos que se almacenadan en la variable ```frames``` y que puede visualizarse con la biblioteca ```matplotlib```.


**Problema 1**

Modificar el archivo [problema-01.py](problema-01.py) y agregar una lógica de toma de decisiones del robot para cumplir el objetivo

**Problema 2**

Programar una métrica de cuantificación de la eficiencia del robot. Por ej. basada en puntos de recompensa: si el robot choca con una pared -10, por cada movimiento -1, abrir una puerta +5, chocar con la puerta -10, alzar una llave/objeto +20, pero desplazarse con el objeto -3, alcanzar el objetivo +200. Se puede implementar modificando el código del proyecto a partir de la lectura de ```obs``` y la decición tomada. Guardar también la cantidad total de pasos empleada.

**Problema 3**

a- En el archivo [gym_minigrid/envs/empty.py](gym_minigrid/envs/empty.py)  modificar la siguiente funcion 
```def _gen_grid(self, width, height):``` para que genere al azar paredes que no colisionen con el agente ni con el objetivo. 

b- Probar la lógica del robot en ese entorno.

**(OPCIONAL, para el 10) Problema 4**

Modificar el archivo [problema-04-opcional.py](problema-04-opcional.py)  y agregar una lógica de toma de decisiones del robot para cumplir el objetivo


### Metodología de trabajo

Para el desarrollo del proyecto se deberán asociar en grupos de dos personas, realizar un fork del proyecto y trabajar en forma conjunta. Se sugiere realizar módulos comunes del procesamiento e interpretación de los datos de la variables ```obs``` y de la implementación de los puntos 2 y 3. Y se pide programar en forma individual la lógica de navegación y toma de decisiones del robot para los diferentes niveles (problemas 1 y 4). La idea es que los robots compitan, y a partir de la  la definición de la métricas de eficiencia se puedan elaborar gráficas y comparar el rendimiento. Cuando el proyecto este listo realizar un ```merge request``` del proyecto o enviar un mail adjuntando el link al repositorio con la implementación final. Exponer en clase. 